//
//  CatalogTableViewCell.m
//  Movies
//
//  Created by Jiri Vanek on 25/02/2018.
//  Copyright © 2018 Jiri. All rights reserved.
//

#import "CatalogTableViewCell.h"

@implementation CatalogTableViewCell

- (void)prepareForReuse
{
    [super prepareForReuse];
    self.posterImageView.image = nil;
}

@end
