//
//  CatalogViewController.h
//  Movies
//
//  Created by Jiri Vanek on 02/03/2018.
//  Copyright © 2018 Jiri. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CatalogViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UISearchResultsUpdating>

@end
