//
//  DataManager.m
//  Movies
//
//  Created by Jiri Vanek on 25/02/2018.
//  Copyright © 2018 Jiri. All rights reserved.
//

#import "DataManager.h"
#import "NetworkService.h"
#import "PopularMoviesModel.h"
#import "VideoListModel.h"
#import "GenreListModel.h"

@interface DataManager ()

@property (strong, nonatomic) NSArray<MovieModel*> *movies;
@property (strong, nonatomic) NSArray<MovieModel*> *filteredMovies;
@property (strong, nonatomic) NSDictionary<NSNumber*, NSString*> *genres;

/// The number of the last loaded page. If 0, no page has been loaded so far.
@property (assign, nonatomic) NSInteger page;

@end


@implementation DataManager

@synthesize movies = _movies;
@synthesize moviesFilter = _moviesFilter;

- (NSArray<MovieModel *> *)movies
{
    // lazy init
    if (!_movies)
    {
        _movies = [NSArray new];
    }
    
    return _movies;
}

- (void)setMovies:(NSArray<MovieModel *> *)movies
{
    // Apart from assingnig movies, also set filteredMovies
    _movies = movies;
    self.moviesFilter = self.moviesFilter; // calls custom setter, which applies the filter -> re-evaluates filteredMovies
}

- (void)setMoviesFilter:(NSString *)moviesFilter
{
    _moviesFilter = moviesFilter;
    
    if (_moviesFilter.length)
    {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"title CONTAINS[cd] %@", moviesFilter];
        NSArray<MovieModel*> *filtered = [self.movies filteredArrayUsingPredicate:predicate];
        self.filteredMovies = filtered;
    }
    else
    {
        self.filteredMovies = [self.movies copy];
    }
}

- (void) loadMoreMoviesWithCompletionBlock:(void (^)(BOOL success))block
{
    [[NetworkService new] getMoviesPage:self.page+1 completionBlock:^(id json) {
        PopularMoviesModel *moviePage;
        if (json)
        {
            NSDictionary *payload = (NSDictionary*)json;
            moviePage = [[PopularMoviesModel alloc] initWithDictionary:payload error:nil];
            self.movies = [self.movies arrayByAddingObjectsFromArray:moviePage.results];
            self.page++;
        }
        BOOL success = moviePage.results.count;
        
        if (success && !self.genres)
        {
            // make sure we also download genre list with the first movie page
            [self loadGenresWithCompletionBlock:^{
                block(success);
            }];
        }
        else
        {
            block(success);
        }
    }];
}

- (void) getVideosForMovie:(NSNumber*)movieID completionBlock:(void (^)(NSArray<VideoModel*> *))block
{
    [[NetworkService new] getVideosForMovie:movieID completionBlock:^(id json) {
        NSDictionary *payload = (NSDictionary*)json;
        VideoListModel *videos = [[VideoListModel alloc] initWithDictionary:payload error:nil];
        block(videos.results);
    }];
}

- (void) loadGenresWithCompletionBlock:(void (^)(void))block
{
    [[NetworkService new] getGenresCompletionBlock:^(id json) {
        NSDictionary *payload = (NSDictionary*)json;
        GenreListModel *genreList = [[GenreListModel alloc] initWithDictionary:payload error:nil];
        
        NSMutableDictionary *dict = [NSMutableDictionary new];
        for (GenreModel *genre in genreList.genres)
        {
            dict[genre.id] = genre.name;
        }
        self.genres = [NSDictionary dictionaryWithDictionary:dict]; //immutable copy
        block();
    }];
}

- (NSString*) genresForIds:(NSArray*)genreIDs
{
    NSString *result = @"";
    for (NSNumber *genreID in genreIDs)
    {
        if (result.length > 0)
        {
            result = [result stringByAppendingString:@", "];
        }
        
        NSString *genreName = self.genres[genreID];
        if (genreName)
        {
            result = [result stringByAppendingString:genreName];
        }
    }
    
    return result;
}

@end
