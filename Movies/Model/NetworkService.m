//
//  NetworkService.m
//  Movies
//
//  Created by Jiri Vanek on 25/02/2018.
//  Copyright © 2018 Jiri. All rights reserved.
//

#import "NetworkService.h"
#import <AFNetworking.h>


NSString * const imageUrlBase = @"http://image.tmdb.org/t/p/w342";
NSString * const apiUrlBase = @"https://api.themoviedb.org/3";

@implementation NetworkService

- (void) getMoviesPage:(NSInteger)page completionBlock:(void (^)(id))block
{
    if ((page < 1) || (page > 1000))
    {
        NSLog(@"[NetworkService] Attempt to load page:%ld out of range 1-1000.", (long)page);
        block(nil);
    }
    
    NSString *urlString = [NetworkService urlForPath:@"/movie/popular"];
    NSMutableDictionary *params = [self paramsBaseDictionary];
    params[@"page"] = @(page);
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:urlString
      parameters:params
        progress:^(NSProgress * _Nonnull downloadProgress) {
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            block(responseObject);
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSLog(@"[NetworkService] Error loading movie list: %@", error.localizedDescription);
            block(nil);
        }];
}

- (void) getVideosForMovie:(NSNumber*)movieID completionBlock:(void (^)(id))block
{
    NSString *path = [NSString stringWithFormat:@"/movie/%@/videos", movieID];
    NSString *urlString = [NetworkService urlForPath:path];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:urlString
      parameters:[self paramsBaseDictionary]
        progress:^(NSProgress * _Nonnull downloadProgress) {
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            block(responseObject);
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSLog(@"[NetworkService] Error loading videos: %@", error.localizedDescription);
            block(nil);
        }];
}

- (void) getGenresCompletionBlock:(void (^)(id))block
{
    NSString *urlString = [NetworkService urlForPath:@"/genre/movie/list"];
    NSMutableDictionary *params = [self paramsBaseDictionary];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:urlString
      parameters:params
        progress:^(NSProgress * _Nonnull downloadProgress) {
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            block(responseObject);
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSLog(@"[NetworkService] Error loading genre list: %@", error.localizedDescription);
            block(nil);
        }];
}


#pragma mark - convenience

/**
 @return A dictionary containing params mandatory for all requests. Currently has only 'api_key'.
 */
- (NSMutableDictionary*) paramsBaseDictionary
{
    NSDictionary *params = @{@"api_key":@"0f5b6aa045800506b2f53f61942f39bd"};
    return params.mutableCopy;
}

+ (NSString*)urlForPath:(NSString*)path
{
    return [apiUrlBase stringByAppendingString:path];
}

+ (NSString*)imageUrlForPath:(NSString*)path
{
    return [imageUrlBase stringByAppendingString:path];
}
    

@end
