//
//  NetworkService.h
//  Movies
//
//  Created by Jiri Vanek on 25/02/2018.
//  Copyright © 2018 Jiri. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 Communication with the server API (themoviedb). Encapulates network calls, returns raw json responses.
 */
@interface NetworkService : NSObject

/**
 Loads 1 page of the movie list.
 @param page - number of the page to load. Must be between 1-1000
 @param block - called when loading completed, provides raw json response
 */
- (void) getMoviesPage:(NSInteger)page completionBlock:(void (^)(id))block;

/**
 Loads videos metadata for the given movie.
 @param movieID - movie identifier
 @param block - called when loading completed, provides raw json response
 */
- (void) getVideosForMovie:(NSNumber*)movieID completionBlock:(void (^)(id))block;

/**
 Loads the list of all genres, which determines mapping of genreID->genreName.
 @param block - called when loading completed, provides raw json response
 */
- (void) getGenresCompletionBlock:(void (^)(id))block;

/**
 @param path - relative path to the resource
 @return Full URL string to the image with the relative path.
 */
+ (NSString*)imageUrlForPath:(NSString*)path;

@end
