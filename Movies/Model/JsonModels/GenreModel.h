//
//  GenreModel.h
//  Movies
//
//  Created by Jiri Vanek on 04/03/2018.
//  Copyright © 2018 Jiri. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface GenreModel : JSONModel

@property (strong, nonatomic) NSNumber *id;
@property (strong, nonatomic) NSString *name;

@end
