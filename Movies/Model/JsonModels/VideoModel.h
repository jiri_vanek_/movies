//
//  VideoModel.h
//  Movies
//
//  Created by Jiri Vanek on 27/02/2018.
//  Copyright © 2018 Jiri. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface VideoModel : JSONModel

@property (strong, nonatomic) NSString *key;

@end
