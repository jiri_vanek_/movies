//
//  GenreListModel.h
//  Movies
//
//  Created by Jiri Vanek on 04/03/2018.
//  Copyright © 2018 Jiri. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@protocol GenreModel;

@interface GenreListModel : JSONModel

@property (strong, nonatomic) NSArray <GenreModel>  *genres;

@end
