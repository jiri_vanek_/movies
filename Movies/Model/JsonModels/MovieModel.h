//
//  MovieModel.h
//  Movies
//
//  Created by Jiri Vanek on 26/02/2018.
//  Copyright © 2018 Jiri. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface MovieModel : JSONModel

@property (strong, nonatomic) NSNumber *id;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString<Optional> *overview;
@property (strong, nonatomic) NSString<Optional> *release_date;
//@property (strong, nonatomic) NSString *poster_path; // Portrait-oriented image. We're using backdrop_path for landscape.
@property (strong, nonatomic) NSString<Optional> *backdrop_path;
@property (strong, nonatomic) NSArray<Optional> *genre_ids;

/**
 Convenience method which provides full URL to the movie poster image.
 @return URL generated from the relative path in 'backdrop_path'
 */
- (NSURL*) posterUrl;

@end
