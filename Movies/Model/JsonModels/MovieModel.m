//
//  MovieModel.m
//  Movies
//
//  Created by Jiri Vanek on 26/02/2018.
//  Copyright © 2018 Jiri. All rights reserved.
//

#import "MovieModel.h"
#import "NetworkService.h"

@implementation MovieModel

- (NSURL*) posterUrl
{
    NSString *fullUrlString = [NetworkService imageUrlForPath:self.backdrop_path];
    return [NSURL URLWithString:fullUrlString];
}

@end

