//
//  VideoListModel.h
//  Movies
//
//  Created by Jiri Vanek on 27/02/2018.
//  Copyright © 2018 Jiri. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@protocol VideoModel;

@interface VideoListModel : JSONModel

@property (strong, nonatomic) NSArray <VideoModel>  *results;

@end
