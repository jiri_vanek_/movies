//
//  PopularMoviesModel.h
//  Movies
//
//  Created by Jiri Vanek on 26/02/2018.
//  Copyright © 2018 Jiri. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@protocol MovieModel;

@interface PopularMoviesModel : JSONModel

@property (strong, nonatomic) NSArray <MovieModel>  *results;

@end
