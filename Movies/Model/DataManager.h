//
//  DataManager.h
//  Movies
//
//  Created by Jiri Vanek on 25/02/2018.
//  Copyright © 2018 Jiri. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MovieModel.h"
#import "VideoModel.h"
#import "GenreModel.h"

/**
 Main data 'model' of the app - manages loading and filtering od all movie data.
 */
@interface DataManager : NSObject

/// List of all the loaded movies
@property (strong, readonly) NSArray<MovieModel*> *movies;

/// Filtered list of movies. Contains only movies, where movie.title contain the phrase in 'moviesFilter'.
@property (strong, readonly) NSArray<MovieModel*> *filteredMovies;

/// Search phrase to filter movie titles. Has a custom setter, which applies the filter on 'movies' -> re-evaluates filteredMovies
@property (strong, nonatomic) NSString *moviesFilter;

/**
 * Loads the list of popular movies. If we have already loaded some, loads next page and adds it to the list.
 * @parma block - called when loading completed, reports if it was successful
 */
- (void) loadMoreMoviesWithCompletionBlock:(void (^)(BOOL success))block;

/**
 Loads videos metadata for the given movie.
 @param movieID - movie identifier
 @param block - called when loading completed, provides the list of videos
 */
- (void) getVideosForMovie:(NSNumber*)movieID completionBlock:(void (^)(NSArray<VideoModel*> *))block;

/**
 Translates list of genre IDs into a human readable string. Is synchronous, because the list of all genres has been downloaded together with the first movie list.
 @param genreIDs - list of genre IDs (numbers).
 @return string composed of the given genres (e.g. "Action, Thriller, Science Fiction").
 */
- (NSString*) genresForIds:(NSArray*)genreIDs;

@end
