//
//  NoConnectionPresenter.m
//  Movies
//
//  Created by Jiri Vanek on 02/03/2018.
//  Copyright © 2018 Jiri. All rights reserved.
//

#import "NoConnectionPresenter.h"
#import "Utils.h"
#import "NoConnectionView.h"

static CGFloat const ANIMATION_TIME = 0.8;


@implementation NoConnectionPresenter

+ (void)presentNoConnectionInViewController:(UIViewController *)vc
{
    [NoConnectionPresenter findNoConnectionViewInView:vc.view completionBlock:^(NoConnectionView *noConnView) {
        if (!noConnView) //if the view not present yet, create & show it. Otherwise do nothing.
        {
            UIView *xibView = [Utils loadViewFromXib:@"NoConnectionView"];
            if ([xibView isKindOfClass:[NoConnectionView class]])
            {
                [NoConnectionPresenter animateSlidingView:xibView intoView:vc.view];
            }
        }
    }];
}

+ (void)hideNoConnectionFromController:(UIViewController*)vc
{
    [NoConnectionPresenter findNoConnectionViewInView:vc.view completionBlock:^(NoConnectionView *noConnView) {
        [NoConnectionPresenter animateHidingView:noConnView];
    }];
}

#pragma mark - Animations, executing on main thread

+ (void)animateSlidingView:(UIView*)subView intoView:(UIView*)parentView
{
    if (!subView || !parentView)
    {
        return;
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [parentView addSubview:subView];
        [subView setTranslatesAutoresizingMaskIntoConstraints:NO];
        
        NSDictionary *views = NSDictionaryOfVariableBindings(subView);
        NSArray *horizontalConstraint = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[subView]|" options:0 metrics:nil views:views];
        NSArray *zeroVerticalConstraint = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[subView(==0)]|" options:0 metrics:nil views:views];
        NSArray *finalVerticalConstraint = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[subView(==60@90)]|" options:0 metrics:nil views:views];
        
        [parentView addConstraints:horizontalConstraint];
        [parentView addConstraints:zeroVerticalConstraint];
        
        [parentView layoutIfNeeded];
        [UIView animateWithDuration:ANIMATION_TIME animations:^{
            [parentView removeConstraints:zeroVerticalConstraint];
            [parentView addConstraints:finalVerticalConstraint];
            [parentView layoutIfNeeded];
        }];
    });
}

+ (void)animateHidingView:(UIView*)view
{
    if (!view)
    {
        return;
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        NSDictionary *views = NSDictionaryOfVariableBindings(view);
        NSArray *zeroVerticalConstraint = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[view(==0@100)]|" options:0 metrics:nil views:views];
        
        [UIView animateWithDuration:ANIMATION_TIME animations:^{
            [view.superview addConstraints:zeroVerticalConstraint];
            [view layoutIfNeeded];
        } completion:^(BOOL finished) {
            [view removeFromSuperview];
        }];
    });
}

+ (void)findNoConnectionViewInView:(UIView*)view completionBlock:(void(^)(NoConnectionView *))block
{
    dispatch_async(dispatch_get_main_queue(), ^{
        NoConnectionView *noConnView;
        for (UIView *subview in view.subviews)
        {
            if ([subview isKindOfClass:[NoConnectionView class]])
            {
                noConnView = (NoConnectionView*)subview;
            }
        }

        block(noConnView);
    });
}

@end
