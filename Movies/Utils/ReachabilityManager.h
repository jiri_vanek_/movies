//
//  ReachabilityManager.h
//  Movies
//
//  Created by Jiri Vanek on 03/03/2018.
//  Copyright © 2018 Jiri. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ReachabilityManager : NSObject

/**
 Singleton provider
 */
+ (instancetype)sharedInstance;

/**
 Checks current reachability status, shows/hides 'noConnection' accordingly.
 */
- (void)checkReachability;

@end
