//
//  ReachabilityManager.m
//  Movies
//
//  Created by Jiri Vanek on 03/03/2018.
//  Copyright © 2018 Jiri. All rights reserved.
//

#import "ReachabilityManager.h"
#import "Utils.h"
#import "NoConnectionPresenter.h"
#import <AFNetworking.h>

@interface ReachabilityManager ()

/// Used as a convenience accessor to [AFNetworkReachabilityManager sharedManager].
@property (strong, nonatomic) AFNetworkReachabilityManager *afManager;

/// Last seen status
@property (assign, nonatomic) AFNetworkReachabilityStatus status;

@end


@implementation ReachabilityManager

+ (instancetype)sharedInstance
{
    static ReachabilityManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[ReachabilityManager alloc] init];
    });
    
    return sharedInstance;
}

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.status = AFNetworkReachabilityStatusUnknown;
        self.afManager = [AFNetworkReachabilityManager sharedManager];
        [self.afManager startMonitoring];
        [self watchReachabilityChanges];
    }
    return self;
}

- (void)watchReachabilityChanges
{
    __weak typeof(self) weakSelf = self;
    [self.afManager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        weakSelf.status = status;
        [weakSelf checkReachability];
    }];
}

- (void)checkReachability
{
    UIViewController *vc = [Utils topViewController];
    
    // checking self.status because afManager.rechable is not reliable on app start/reachability start
    // also checking afManager.reachable because self.status depends on rechability change events, which sometimes don't get delivered
    if (self.status == AFNetworkReachabilityStatusNotReachable && !self.afManager.reachable)
    {
        // Show 'noConnection'
        [NoConnectionPresenter presentNoConnectionInViewController:vc];
    }
    else
    {
        // Hide an eventual 'noConnection' view
        [NoConnectionPresenter hideNoConnectionFromController:vc];
    }
}

@end
