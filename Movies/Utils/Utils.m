//
//  Utils.m
//  Movies
//
//  Created by Jiri Vanek on 26/02/2018.
//  Copyright © 2018 Jiri. All rights reserved.
//

#import "Utils.h"
#import <AFNetworking/UIImageView+AFNetworking.h>


@implementation Utils

+ (void)setupImageView:(UIImageView*)imageView withUrl:(NSURL *)imageUrl
{
    NSURLRequest *imageRequest = [NSURLRequest requestWithURL:imageUrl];
    __weak UIImageView *imageViewWeak = imageView;
    [imageView setImageWithURLRequest:imageRequest
                     placeholderImage:nil
                              success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
                                  imageViewWeak.image = [image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
                              } failure:nil];
}

+ (UIViewController*)topViewController
{
    UIViewController *rootVC = [UIApplication sharedApplication].keyWindow.rootViewController;
    if ([rootVC isKindOfClass:[UISplitViewController class]])
    {
        UISplitViewController *splitVC = (UISplitViewController*)rootVC;
        UIViewController *masterVC = [splitVC.viewControllers firstObject];
        if ([masterVC isKindOfClass:[UINavigationController class]])
        {
            UINavigationController *navController = (UINavigationController*)masterVC;
            return navController.topViewController;
        }
    }
    
    // Unexpected controller hierarchy
    return nil;
}

#pragma mark - storyboard/XIB loading

+ (UIViewController*)loadViewControllerWithID:(NSString*)storyboardID
{
    @try {
        NSBundle *mainBundle = [NSBundle mainBundle];
        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:mainBundle];
        return [storyboard instantiateViewControllerWithIdentifier:storyboardID];
    } @catch (__unused NSException *exception) {
        NSLog(@"[Utils] Failed to load ViewController: %@", storyboardID);
        return nil;
    }
}

+ (UIView *)loadViewFromXib:(NSString *)xibName
{
    NSBundle *mainBundle = [NSBundle mainBundle];
    NSArray *elements = [mainBundle loadNibNamed:xibName owner:nil options:nil];
    for (id object in elements)
    {
        if ([object isKindOfClass:[UIView class]])
        {
            return object;
        }
    }
    
    return nil;
}

#pragma mark - Loading screen

+ (void)showLoadingInController:(UIViewController*)vc
{
    [Utils hideLoadingFromController:vc];
    UIViewController *loadingVC = [Utils loadViewControllerWithID:@"LoadingViewController"];
    [vc presentViewController:loadingVC animated:NO completion:nil];
}

+ (void)hideLoadingFromController:(UIViewController*)vc
{
    UIViewController *loadingVC = vc.presentedViewController;
    [loadingVC dismissViewControllerAnimated:NO completion:nil];
}

#pragma mark - fortmatting

+ (NSString*)reformatDate:(NSString*)date
{
    NSDateFormatter *formatter = [NSDateFormatter new];
    formatter.dateFormat = @"yyyy-MM-dd";
    NSDate *nsdate = [formatter dateFromString:date];
    formatter.dateFormat = @"dd.MM.yyyy";
    NSString *result = [formatter stringFromDate:nsdate];
    return result;
}


@end
