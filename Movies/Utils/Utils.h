//
//  Utils.h
//  Movies
//
//  Created by Jiri Vanek on 26/02/2018.
//  Copyright © 2018 Jiri. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

/**
 Miscellaneous helper methods.
 */
@interface Utils : NSObject

/**
 Setup an imageView to load & show an image from the given URL.
 @param imageView - view that will show the image
 @param imageUrl - URL to the image to be displayed
 */
+ (void)setupImageView:(UIImageView*)imageView withUrl:(NSURL *)imageUrl;

/**
 Searches for the top (visible) controller in our naviagation stack.
 @return a visible viewcontroller, or nil in case of some unexpected controller hierarchy (error)
 */
+ (UIViewController*)topViewController;

/**
 Loads a ViewController from the Main storyboard.
 @param storyboardID - storyboard ID of the requested ViewController
 @return An viewController with the given name, or nil in case of an error.
 */
+ (UIViewController*)loadViewControllerWithID:(NSString*)storyboardID;

/**
 Loads a view from the given XIB.
 @param xibName name of the xib file (without extension)
 @return view instantiated from the xib
 */
+ (UIView *)loadViewFromXib:(NSString *)xibName;

/**
 Modally presents a screen with an activity indicator. It transparently covers the whole area of the given VC.
 @param vc - Viewcontroller to present the loading screen.
 */
+ (void)showLoadingInController:(UIViewController*)vc;

/**
 Hides the previously displayed loading screen (if there was one).
 @param vc - Viewcontroller which originally presented the loading screen.
 */
+ (void)hideLoadingFromController:(UIViewController*)vc;

/**
 Converts date from  "yyyy-MM-dd" to "dd.MM.yyyy"
 @param date - to be converted. 
 @return the same date in a new format
 */
+ (NSString*)reformatDate:(NSString*)date;

@end
