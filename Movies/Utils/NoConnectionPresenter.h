//
//  NoConnectionPresenter.h
//  Movies
//
//  Created by Jiri Vanek on 02/03/2018.
//  Copyright © 2018 Jiri. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class NoConnectionView;

@interface NoConnectionPresenter : NSObject

/**
 @brief Creates new 'noConnection' view, animates the presentation.
 @param vc - the ViewController into which the new 'noConnetion' will be presented
 */
+ (void)presentNoConnectionInViewController:(UIViewController *)vc;

/**
 @brief Animates hiding of a 'noConnetion' view.
 @param vc - ViewController which will be searched for a 'noConnection' view. If found, it will be removed from the VC.
 */
+ (void)hideNoConnectionFromController:(UIViewController*)vc;

@end
