//
//  VideoManager.m
//  Movies
//
//  Created by Jiri Vanek on 27/02/2018.
//  Copyright © 2018 Jiri. All rights reserved.
//

#import "VideoManager.h"
#import <XCDYouTubeKit/XCDYouTubeKit.h>

@implementation VideoManager

+ (void)getStreamUrlForVideo:(NSString*)videoID completionBlock:(void (^)(NSURL *))block
{
    [[XCDYouTubeClient defaultClient] getVideoWithIdentifier:videoID completionHandler:^(XCDYouTubeVideo * _Nullable video, NSError * _Nullable error) {
        if (video)
        {
            NSDictionary *streamURLs = video.streamURLs;
            NSURL *streamURL = streamURLs[XCDYouTubeVideoQualityHTTPLiveStreaming] ?: streamURLs[@(XCDYouTubeVideoQualityHD720)] ?: streamURLs[@(XCDYouTubeVideoQualityMedium360)] ?: streamURLs[@(XCDYouTubeVideoQualitySmall240)];
            block(streamURL);
        }
        else
        {
            NSLog(@"[VideoManager] Failed to download stream URL: %@", error.localizedDescription);
            block(nil);
        }
    }];
}


@end
