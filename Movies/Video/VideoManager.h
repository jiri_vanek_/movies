//
//  VideoManager.h
//  Movies
//
//  Created by Jiri Vanek on 27/02/2018.
//  Copyright © 2018 Jiri. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface VideoManager : NSObject

/**
 Gets a stream URL for a youtube video.
 @param videoID - ID of the requested youtube video. Expects 11 characters, e.g. "SUXWAEX2jlg"
 @param block - called when loading completed, provides the stream URL, or nil in case of an error
 */
+ (void)getStreamUrlForVideo:(NSString*)videoID completionBlock:(void (^)(NSURL *))block;

@end
