//
//  AppDelegate.h
//  Movies
//
//  Created by Jiri Vanek on 25/02/2018.
//  Copyright © 2018 Jiri. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

