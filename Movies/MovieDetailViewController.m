//
//  MovieDetailViewController.m
//  Movies
//
//  Created by Jiri Vanek on 25/02/2018.
//  Copyright © 2018 Jiri. All rights reserved.
//

#import "MovieDetailViewController.h"
#import "DataManager.h"
#import "MovieModel.h"
#import "Utils.h"
#import "NoConnectionPresenter.h"
#import "VideoManager.h"
#import "ReachabilityManager.h"
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>
#import <SafariServices/SafariServices.h>

@interface MovieDetailViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *genreLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *overviewLabel;
@property (weak, nonatomic) IBOutlet UIButton *trailerButton;

@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *allLabels;

@end


@implementation MovieDetailViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    [self setupUI];
    [[ReachabilityManager sharedInstance] checkReachability];
}

- (void)setMovie:(MovieModel *)movie
{    
    _movie = movie;
    [self setupUI];
}

- (void)setupUI
{
    // hide UI elements when we don't have a selected movie
    BOOL hide = !self.movie;
    self.imageView.hidden = hide;
    self.trailerButton.hidden = hide;
    for (UILabel *label in self.allLabels)
    {
        label.hidden = hide;
    }
    
    if (!hide)
    {
        self.titleLabel.text = self.movie.title;
        self.dateLabel.text = [Utils reformatDate: self.movie.release_date];
        self.overviewLabel.text = self.movie.overview;
        self.genreLabel.text = [self.dataManager genresForIds:self.movie.genre_ids];
        [Utils setupImageView:self.imageView withUrl:[self.movie posterUrl]];
    }
}

- (IBAction)watchTrailerPressed:(id)sender
{
    [Utils showLoadingInController:self];
    
    [self.dataManager getVideosForMovie:self.movie.id completionBlock:^(NSArray<VideoModel *> *movies) {
        if (movies.count)
        {
            VideoModel *videoModel = movies.firstObject;
            [self playVideo:videoModel.key];
        }
        else
        {
            [Utils hideLoadingFromController:self];
            [[ReachabilityManager sharedInstance] checkReachability];
            // TODO: no movies available -> tell the user
        }
    }];
}

- (void)playVideo:(NSString*)videoID
{
    [VideoManager getStreamUrlForVideo:videoID completionBlock:^(NSURL *streamURL) {
        
        [Utils hideLoadingFromController:self];
        
        if (!streamURL)
        {
            return;
        }
        
        // MPMoviePlayerViewController, used by default in XCDYouTubeKit, is deprecated.
        // AVPlayerViewController doesn't work for Youtube streams.
        // => We'll use SafariViewController as a workaround
        /*
         AVPlayerViewController *playerViewController = [AVPlayerViewController new];
         [vc presentViewController:playerViewController animated:YES completion:nil];
         playerViewController.player = [AVPlayer playerWithURL:streamURL];
         [playerViewController.player play];
         */
        
        SFSafariViewController *safariController = [[SFSafariViewController alloc] initWithURL:streamURL];
        [self presentViewController:safariController animated:YES completion:nil];
    }];
}


@end
