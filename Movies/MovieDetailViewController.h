//
//  MovieDetailViewController.h
//  Movies
//
//  Created by Jiri Vanek on 25/02/2018.
//  Copyright © 2018 Jiri. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MovieModel;
@class DataManager;

@interface MovieDetailViewController : UIViewController

/// Model of the currently selected movie
@property (strong, nonatomic) MovieModel *movie;

/// Data manager instance, which has already loaded the movie list & genre list.
@property (weak, nonatomic) DataManager *dataManager;

@end
