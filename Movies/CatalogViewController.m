//
//  CatalogViewController.m
//  Movies
//
//  Created by Jiri Vanek on 25/02/2018.
//  Copyright © 2018 Jiri. All rights reserved.
//

#import "CatalogViewController.h"
#import "CatalogTableViewCell.h"
#import "DataManager.h"
#import "Utils.h"
#import "MovieDetailViewController.h"
#import "NoConnectionPresenter.h"
#import "ReachabilityManager.h"

@interface CatalogViewController ()

@property (strong, nonatomic) DataManager *dataManager;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) UISearchController *searchController;

@end


@implementation CatalogViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupSearch];
    [self loadMoreMovies];
}

- (void)viewWillAppear:(BOOL)animated
{
    [[ReachabilityManager sharedInstance] checkReachability];
}

- (DataManager *)dataManager
{
    if (!_dataManager)
    {
        _dataManager = [DataManager new];
    }
    
    return _dataManager;
}

- (void)setupSearch
{
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.searchResultsUpdater = self;
    self.searchController.searchBar.placeholder = @"Filter";
    self.searchController.dimsBackgroundDuringPresentation = NO;
    
    self.tableView.tableHeaderView = self.searchController.searchBar;
    self.definesPresentationContext = YES;
}

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController
{
    self.dataManager.moviesFilter = searchController.searchBar.text;
    [self.tableView reloadData];
}


#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataManager.filteredMovies.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CatalogTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"catalogCell" forIndexPath:indexPath];
    
    MovieModel *movie = self.dataManager.filteredMovies[indexPath.row];
    cell.titleLabel.text = movie.title;
    
    NSURL *imageUrl = [movie posterUrl];
    [Utils setupImageView:cell.posterImageView withUrl:imageUrl];
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.dataManager.moviesFilter.length || self.searchController.view.superview)
    {
        // Don't load new page when filter is active
        return;
    }
    
    // If scrolled to the end, load more movies
    if (indexPath.row == self.dataManager.filteredMovies.count-1)
    {
        [self loadMoreMovies];
    }
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.destinationViewController isKindOfClass:[MovieDetailViewController class]])
    {
        MovieDetailViewController *movieVC = (MovieDetailViewController*)segue.destinationViewController;
        movieVC.movie = self.dataManager.filteredMovies[self.tableView.indexPathForSelectedRow.row];
        movieVC.dataManager = self.dataManager;
    }
}

#pragma mark - Convenience

- (void)loadMoreMovies
{
    if (self.view.superview) // if this view is already in window hierarchy
    {
        [Utils showLoadingInController:self];
    }
    
    [self.dataManager loadMoreMoviesWithCompletionBlock:^(BOOL success) {
        if (success)
        {
            [self.tableView reloadData];
        }
        else
        {
            [[ReachabilityManager sharedInstance] checkReachability];
        }
        
        [Utils hideLoadingFromController:self];
    }];
}


@end

