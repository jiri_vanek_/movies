//
//  MovieDetailViewControllerTests.m
//  MoviesTests
//
//  Created by Jiri Vanek on 05/03/2018.
//  Copyright © 2018 Jiri. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "MovieDetailViewController.h"
#import "MovieModel.h"
#import "Utils.h"

@interface MovieDetailViewController ()

// Access to private properties
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *genreLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *overviewLabel;
@property (weak, nonatomic) IBOutlet UIButton *trailerButton;

@end


@interface MovieDetailViewControllerTests : XCTestCase

@property (strong, nonatomic) MovieDetailViewController *sut; // System Under Test
@property (strong, nonatomic) MovieModel *movie;

@end


@implementation MovieDetailViewControllerTests

- (void)setUp
{
    [super setUp];
    
    NSDictionary *movieDict = @{@"id":@(297761),
                                @"title":@"Suicide Squad",
                                @"overview":@"From DC Comics comes the Suicide Squad...",
                                @"release_date":@"2016-08-03",
                                @"backdrop_path":@"/ndlQ2Cuc3cjTL7lTynw6I4boP4S.jpg",
                                @"genre_ids":@[@(14),@(28),@(80)]
                                };
    self.movie = [[MovieModel alloc] initWithDictionary:movieDict error:nil];
    
    self.sut = (MovieDetailViewController*)[Utils loadViewControllerWithID:@"MovieDetailViewController"];
    UIView *view = self.sut.view; // the getter loads view, creates outlets
    XCTAssertTrue(view); // just make sure the view has beed loaded
}

- (void)tearDown
{
    [super tearDown];
}

- (void)testHidingUIOnStartup
{
    // VC appears, no movie has been set
    [self.sut viewWillAppear:NO];
    
    XCTAssertTrue(self.sut.imageView.hidden);
    XCTAssertTrue(self.sut.titleLabel.hidden);
    XCTAssertTrue(self.sut.genreLabel.hidden);
    XCTAssertTrue(self.sut.dateLabel.hidden);
    XCTAssertTrue(self.sut.overviewLabel.hidden);
    XCTAssertTrue(self.sut.trailerButton.hidden);
}

- (void)testUIVisibleWhenMovieSet
{
    // VC appears
    [self.sut viewWillAppear:NO];
    
    // Movie setter should un-hide the UI
    self.sut.movie = self.movie;
    
    XCTAssertFalse(self.sut.imageView.hidden);
    XCTAssertFalse(self.sut.titleLabel.hidden);
    XCTAssertFalse(self.sut.genreLabel.hidden);
    XCTAssertFalse(self.sut.dateLabel.hidden);
    XCTAssertFalse(self.sut.overviewLabel.hidden);
    XCTAssertFalse(self.sut.trailerButton.hidden);
}

- (void)testLabels
{
    // Movie setter should fill all labels
    self.sut.movie = self.movie;
    
    XCTAssertEqual(self.sut.titleLabel.text, @"Suicide Squad");
    XCTAssertEqual(self.sut.overviewLabel.text, @"From DC Comics comes the Suicide Squad...");
    XCTAssertTrue([self.sut.dateLabel.text isEqualToString:@"03.08.2016"]); // dateLabel has been converted and is a different data type internally -> we must use isEqualToString:
}

@end
